import { Servidor } from '../src/Servidor';
import { PORT_TEST } from '../src/utils/config'
import * as http from 'http';

const request = require('supertest')
const app = new Servidor(PORT_TEST);
let server:http.Server;
jest.setTimeout(6000);

// Se inicia el servidor para comenzar a escuchar peticiones
beforeEach(async done => {
    server = await app.listen();
    done()
})

// despues de cada prueba se cierra el servidor iniciado en beforeEach
afterEach(done => {
    return server && server.close(done)
  })

// Server
describe('Pruebas para Api Persona', function () {

  test('get status api', function (done) {
    request(app.app)
      .get('/api-persona-desa/status')
      .expect(200)
      .end(function (err: any, res: any) {
        if (err) return fail(err)
        done()
      })
  })

  test('get data as400 api', function (done) {
    request(app.app)
      .get('/api-persona-desa/1')
      .expect(200)
      .end(function (err: any, res: any) {
        if (err) return fail(err)
        done()
      })
  })


  test('post test admin',  function (done) {
    const body = {
          "nombre":"admin",
          "clave": "admin"
      }
     request(app.app)
      .post('/api-persona-desa/')
      .send(body)
      .expect(200)
      .end( async function (err: any, res: any) {
        if (err) return fail(err)
        done()
    })
  })

  test('post test admin FAIL',  function (done) {
    const body = {
      "nombre":"admin",
      "clave": "ABC"
    }
     request(app.app)
      .post('/api-persona-desa/')
      .send(body)
      .expect(400)
      .end( async function (err: any, res: any) {
        if (err) return fail(err)
        done()
    })
  })

})
