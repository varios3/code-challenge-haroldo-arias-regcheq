# Code Challenge Haroldo Arias

Api desarrollada en TypeScript, se tiene todas las depedencias necesarias para poderse ejecutar.

A continuación se muestran los pasos para correr el API y hacer las pruebas 

## npm

Para correr tanto el api y las pruebas es necesario instalar las depedencias del código:

### Depedencias
    
        npm i

### Correr API
    
        npm run start

### Correr Pruebas

        npm run test

### Generar la página del Coverage de las Pruebas

        npm run coverage


### Generar la versión de distribución en código JavaScript

        npm run build


El api tiene 4 archivos de entorno, el principal el .env, unicamente contiene el ambiente en que se esta corriendo, tiene por defecto desarrrollo y el puerto 3000

Por lo que si se desea modificar algun valor sobre el puerto o ruta modificar el archivo .env.desarrollo



## End Points

Se detallan los 2 End Points que tiene el Servicio

### Obtener Persona  (GET)
    
        http://localhost:3000/api-persona-desa/:id_persona

### Validar Admin   (POST)
    
        http://localhost:3000/api-persona-desa

#### Body

```json
{
    "nombre": "admin",
    "clave": "333"
}
```

