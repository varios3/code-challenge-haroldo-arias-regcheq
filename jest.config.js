module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  coverageDirectory: 'coverage',
  testPathIgnorePatterns: [
    "/node_modules"
  ],
  verbose: true,
  testResultsProcessor: 'jest-sonar-reporter',
  testMatch: ['<rootDir>/__test__/**/*.test.ts'],
  coverageThreshold: {
    global: {
      branches: 33.3,
      functions: 100,
      lines: 60,
      statements: 60
    }
  }
};


