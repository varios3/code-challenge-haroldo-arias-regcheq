import {Request , Response} from 'express';
import { Excepcion } from '../interface/Excepcion';
import { Persona } from '../models/Persona';

export function indexStatus(req:Request, res:Response): Response{
    return res.json('DEMO');
}

export async function validarPersona(req:Request, res:Response): Promise<Response>{
    const excepcion:Excepcion = {
        Message: ''
    };

    try{
        if(req.body.nombre && req.body.clave){
            let item: Persona = new Persona(0,req.body.nombre);
            const result = await item.validarRegistro(req.body.clave);
            if(result){
                return res.json({ 
                    Message: `Bienvenido ${req.body.nombre}`
                });
            }
            excepcion.Message = 'Error validando Persona'
        }else{
            excepcion.Message = 'Request no Valido!'
        }
    }catch(error:any){
        excepcion.Message = error
    }

    return res
            .status(400)
            .json(excepcion)
}

export async function getPersona(req:Request, res:Response): Promise<Response> {
    const excepcion:Excepcion = {
        Message: ''
    };

    if(req.params.id) {
        try {
            const item:Persona = new Persona(Number(req.params.id),'');
            const validador = await item.existeRegistro();
            // se valida que exista la linea
            if(validador) {
                return res.json(item); 
            } else {
                // se genera excepción
                excepcion.Message = 'Persona no existe!'
            }
        } catch (error:any) {
            console.log(error);
            excepcion.Message = error
        }
    } else {
        excepcion.Message = 'Parametro Id Requerido'
    }

    return res
            .status(400)
            .json(excepcion)
}


