
export class Persona{

    public id:number;
    public nombre:string;
    public apellido:string;
    
    constructor(id:number,name:string){
        this.id = id;
        this.nombre = name;
        this.apellido = "";
    }
    
    async existeRegistro(): Promise<Boolean>{
        const registros = [
            {
                id: 1,
                nombre: "Haroldo",
                apellido: "Arias"
            },
            {
                id: 2,
                nombre: "Pablo",
                apellido: "Molina"
            },
            {
                id: 3,
                nombre: "Cristián",
                apellido: "Neely"
            },
        ];

        const item = registros.filter(persona => persona.id === this.id);
        if(item.length > 0){
            this.nombre = item[0].nombre;
            this.apellido = item[0].apellido;
            return true;
        }
        
        return false;
    }
    
    async validarRegistro(clave:string): Promise<Boolean>{
        if(this.nombre ==="admin" && clave === "admin" )
            return true;
        return false;
    }

}