import { Router } from 'express';
import {getPersona, validarPersona, indexStatus} from '../controllers/index.controller';
const router = Router();

router.route('/status').get(indexStatus);
router.route('/:id').get(getPersona);
router.route('/').post(validarPersona);

export default router;