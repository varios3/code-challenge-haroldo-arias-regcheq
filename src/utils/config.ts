import * as dotenv from "dotenv";

dotenv.config();
let path;
switch (process.env.NODE_ENV) {
  case "qa":
    path = `${__dirname}/../../.env.qa`;
    break;
  case "master":
    path = `${__dirname}/../../.env.produccion`;
    break;
  default:
    path = `${__dirname}/../../.env.desarrollo`;
}

dotenv.config({ path: path });
export const PORT = process.env.PORT;
export const PORT_TEST = process.env.PORT_TEST;
export const RUTA = process.env.RUTE_ENV || '';